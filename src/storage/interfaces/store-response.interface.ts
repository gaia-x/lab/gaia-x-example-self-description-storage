export interface StoreResponse {
  success: boolean
  id: string
}
