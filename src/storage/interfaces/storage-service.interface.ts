export interface StorageService {
  get(id: string): Promise<object>
  store(sd: object): Promise<boolean>
  list(): Promise<string[]>
  delete(id: string): Promise<boolean>
  deleteOlderThan(timeInMilliseconds: number): Promise<boolean>
}
