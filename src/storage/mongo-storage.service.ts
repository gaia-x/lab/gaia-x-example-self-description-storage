import { ConflictException, Injectable, InternalServerErrorException, Logger } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { SelfDescription, SelfDescriptionDocument } from '../self-descriptions/schemas/self-description.schema'
import { StorageService } from './interfaces/storage-service.interface'

@Injectable()
export class MongoStorageService implements StorageService {
  constructor(@InjectModel(SelfDescription.name) private selfDescriptionModel: Model<SelfDescriptionDocument>) {}
  private readonly logger = new Logger(MongoStorageService.name)

  /* StorageService functions */
  async get(id: string): Promise<object> {
    return await this.findOne(id.toLowerCase())
  }

  async store(sd: SelfDescriptionDocument): Promise<boolean> {
    const createdSelfDescription = await this.create(sd)
    return !!createdSelfDescription
  }

  async list(): Promise<string[]> {
    return await this.findAllIdentifiers()
  }

  async delete(id: string): Promise<boolean> {
    return await this.deleteSdDocument(id)
  }

  async deleteOlderThan(timeInMilliseconds: number): Promise<boolean> {
    return await this.deleteSdDocsOlderThan(timeInMilliseconds)
  }

  /* MongoStorage specific functions */
  private async create(sd: SelfDescriptionDocument): Promise<SelfDescriptionDocument> {
    try {
      const existingDocInDb = await this.selfDescriptionModel.exists({ sha256Hash: sd.sha256Hash })
      if (existingDocInDb) throw new ConflictException({ statusCode: 409, message: 'Entry already exists', error: 'Conflict', id: sd.sha256Hash })

      const createdSelfDescription = await this.selfDescriptionModel.create(sd)
      return createdSelfDescription
    } catch (error) {
      if (error.status === 409) throw error
      this.logger.error(error)
      throw new InternalServerErrorException('Database error')
    }
  }

  private async findAllIdentifiers(): Promise<string[]> {
    try {
      return await this.selfDescriptionModel.distinct('sha256Hash')
    } catch (error) {
      this.logger.error(error)
      throw new InternalServerErrorException('Database error')
    }
  }

  private async findOne(sha256Hash: string): Promise<object> {
    try {
      const sdDoc = await this.selfDescriptionModel.findOne({ sha256Hash }).exec()
      return sdDoc.credentialSubject
    } catch (error) {
      this.logger.error(error)
      throw new InternalServerErrorException('Database error')
    }
  }

  private async deleteSdDocument(sha256Hash: string) {
    try {
      const result = await this.selfDescriptionModel.deleteOne({ sha256Hash }).exec()
      return !!result?.acknowledged
    } catch (error) {
      this.logger.error(error)
      throw new InternalServerErrorException('Database error')
    }
  }

  private async deleteSdDocsOlderThan(timeInMilliseconds: number) {
    try {
      const result = await this.selfDescriptionModel.deleteMany({ createdAt: { $lt: new Date(Date.now() - timeInMilliseconds) } }).exec()
      return !!result?.acknowledged
    } catch (error) {
      this.logger.error(error)
      throw new InternalServerErrorException('Database error')
    }
  }
}
