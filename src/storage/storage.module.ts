import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { PROVIDER_TYPES } from 'src/constants'
import { SelfDescription, SelfDescriptionSchema } from 'src/self-descriptions/schemas/self-description.schema'
import { MongoStorageService } from './mongo-storage.service'

const storageServiceProvider = {
  provide: PROVIDER_TYPES.STORAGE_SERVICE,
  useClass: MongoStorageService // Possible to add switch here to provide other StorageServices (S3StorageService, ...)
}

@Module({
  imports: [MongooseModule.forFeature([{ name: SelfDescription.name, schema: SelfDescriptionSchema }])],
  providers: [storageServiceProvider],
  exports: [storageServiceProvider]
})
export class StorageModule {}
