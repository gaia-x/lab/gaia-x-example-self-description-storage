import { Controller, Get, Post, Body, Param, UsePipes, UseGuards, Delete } from '@nestjs/common'
import { SelfDescriptionsService } from './self-descriptions.service'
import { JoiValidationPipe } from './pipes'
import { SignedSelfDescriptionSchema } from './schemas/joi-self-description.schema'
import { AuthGuard } from '@nestjs/passport'

@Controller('self-descriptions')
export class SelfDescriptionsController {
  constructor(private readonly selfDescriptionsService: SelfDescriptionsService) { }

  @Post()
  @UseGuards(AuthGuard('api-key'))
  @UsePipes(new JoiValidationPipe(SignedSelfDescriptionSchema))
  create(@Body() selfDescription: object) {
    return this.selfDescriptionsService.create(selfDescription)
  }

  @Get()
  findAll() {
    return this.selfDescriptionsService.findAll()
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.selfDescriptionsService.findOne(id)
  }

  @Delete(':id')
  @UseGuards(AuthGuard('api-key'))
  deleteOne(@Param('id') id: string) {
    return this.selfDescriptionsService.delete(id)
  }
}
