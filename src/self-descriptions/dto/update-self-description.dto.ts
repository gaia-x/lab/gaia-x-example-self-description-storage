import { PartialType } from '@nestjs/mapped-types'
import { CreateSelfDescriptionDto } from './create-self-description.dto'

export class UpdateSelfDescriptionDto extends PartialType(CreateSelfDescriptionDto) {}
