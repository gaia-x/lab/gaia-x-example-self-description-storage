import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, Schema as MongooseSchema } from 'mongoose'

export type SelfDescriptionDocument = SelfDescription & Document

@Schema({ timestamps: true })
export class SelfDescription {
  @Prop({ unique: true, required: true, type: MongooseSchema.Types.String })
  sha256Hash: string

  @Prop({ required: true, type: MongooseSchema.Types.Mixed })
  credentialSubject: object

  @Prop({ type: MongooseSchema.Types.String })
  credentialUrl?: string
}

export const SelfDescriptionSchema = SchemaFactory.createForClass(SelfDescription)
