import { Inject, Injectable, Logger } from '@nestjs/common'
import { Cron, CronExpression } from '@nestjs/schedule'
import { createHash } from 'crypto'
import { PROVIDER_TYPES, SD_LIFETIME } from 'src/constants'
import { StorageService } from 'src/storage/interfaces/storage-service.interface'
import { StoreResponse } from 'src/storage/interfaces/store-response.interface'
import { SelfDescription } from './schemas/self-description.schema'

@Injectable()
export class SelfDescriptionsService {
  constructor(@Inject(PROVIDER_TYPES.STORAGE_SERVICE) private storageService: StorageService) {}
  private readonly logger = new Logger(SelfDescriptionsService.name)

  async create(credentialSubject: object): Promise<StoreResponse> {
    const sha256Hash = this.sha256(JSON.stringify(credentialSubject))
    const selfDescriptionDocument: SelfDescription = {
      sha256Hash,
      credentialSubject: credentialSubject
    }
    const isStored = await this.storageService.store(selfDescriptionDocument)
    return {
      success: isStored,
      id: sha256Hash
    }
  }

  findAll() {
    return this.storageService.list()
  }

  findOne(id: string): object {
    return this.storageService.get(id)
  }

  sha256(input: string): string {
    return createHash('sha256').update(input).digest('hex')
  }

  delete(id: string) {
    this.storageService.delete(id)
  }

  @Cron(CronExpression.EVERY_DAY_AT_4AM)
  async deleteOlderSelfDescriptions() {
    this.logger.log('====== Start Cronjob: deleting old Self Descriptions ======')
    const hasSucceded = await this.storageService.deleteOlderThan(SD_LIFETIME)
    if (hasSucceded) {
      this.logger.log('====== Successfully finished Cronjob: deleting old Self Descriptions ======')
      return
    }
    this.logger.error('====== FAILED Cronjob: deleting old Self Descriptions ======')
  }
}
