import { Module } from '@nestjs/common'
import { SelfDescriptionsService } from './self-descriptions.service'
import { SelfDescriptionsController } from './self-descriptions.controller'
import { StorageModule } from 'src/storage/storage.module'
@Module({
  imports: [StorageModule],
  controllers: [SelfDescriptionsController],
  providers: [SelfDescriptionsService]
})
export class SelfDescriptionsModule {}
