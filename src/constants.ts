export const DID_WEB_PATTERN = /^(did:web:)([a-zA-Z0-9%._-]*:)*[a-zA-Z0-9%._-]+$/
export const SD_LIFETIME = 6 * 30 * 24 * 60 * 60 * 1000 // 6 months in milliseconds
export const PROVIDER_TYPES = {
  STORAGE_SERVICE: 'STORAGE_SERVICE'
}
